FROM xmelayx/python-pillow:latest

WORKDIR /bot_telegram

RUN pip3 install pyTelegramBotAPI \
                 pyowm \
                 yahoo-weather \
                 Pillow \
                 py-postgresql

COPY ./ ./

ENTRYPOINT ["python3", "start.py"]
