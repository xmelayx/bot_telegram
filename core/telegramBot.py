import telebot
from telebot import types
import core.setings, core.weatherApi, core.autorizate
import core.tools.chatGenerate as chat
import core.tools.configGenerate as config
import time

TEAM_USER_ACCEPTED = 1
TEAM_USER_LOGGING = 0
user_step = {}

bot = telebot.TeleBot(core.setings.telegramApi)
button = ""
@bot.message_handler(commands=["Chatid", "AddCity", "r"])
def get_chat(message):

    bot.send_message(message.chat.id, text=chat.generateSettingMenu(message))
    user_step[message.chat.id] = TEAM_USER_LOGGING

@bot.message_handler(func=lambda message: user_step.get(message.chat.id) == TEAM_USER_LOGGING)
def addCity(message):
    bot.send_message(message.chat.id, text=config.add_nevCyti(message))
    print(message)
    user_step[message.chat.id] = TEAM_USER_ACCEPTED

@bot.message_handler(commands="Menu")
def get_calendar(message):

    bot.send_message(message.chat.id, "Menu", reply_markup=chat.keyboadr(core.setings.menu))

@bot.callback_query_handler(func=lambda c: True)
def process_callback_button1(callback_query: types.CallbackQuery):
    global tools, button
    try:
        tools, button = chat.generateMenu(callback_query.data, callback_query.from_user.id)
    except:
        print("No buoton")

    if button == "button":
        bot.answer_callback_query(callback_query.id,
                                  text=f"{tools}", show_alert=True)
        button = "no"
    else:
        try:
            if callback_query.data in core.setings.menu or callback_query.data in core.setings.tools:
                bot.edit_message_text(chat_id=callback_query.from_user.id,
                                      message_id=callback_query.message.message_id, text=f"{callback_query.data}",
                                      reply_markup=chat.generateMenu(callback_query.data, callback_query.from_user.id))
        except:

            bot.send_message(callback_query.from_user.id, "Settings",
                             reply_markup=chat.generateMenu(callback_query.data, callback_query.from_user.id))
        if callback_query.data in "Back":
            bot.edit_message_text(chat_id=callback_query.from_user.id, message_id=callback_query.message.message_id,
                                  text=f"{callback_query.data}", reply_markup=chat.keyboadr(core.setings.menu))

def startTelegramBot():
    try:
        bot.polling(none_stop=True, interval=0)
        time.sleep(20)
    except:
        print("I`m restart")
        startTelegramBot()
        time.sleep(20)
    print("dsadasdsa")
