from PIL import Image
from PIL import ImageDraw, ImageFont
def crateImage(tempMax, tempMin, tempNow, codeWaether, day, wind, humidity, city) :
    background = Image.open('core/images/fonss.jpg')

    images = {
        3:  Image.open("core/images/groza-thunder-lightning-storm-icon.png"),
        4: Image.open("core/images/groza-thunder-lightning-storm-icon.png"),
        5: Image.open("core/images/sne-snow-icon.png"),
        6: Image.open("core/images/sne-snow-icon.png"),
        7: Image.open("core/images/sne-snow-icon.png"),
        8: Image.open("core/images/sne-snow-icon.png"),
        9: Image.open("core/images/dozhdlivo-sleet-icon.png"),
        10: Image.open("core/images/sne-snow-icon.png"),
        11: Image.open("core/images/dozhd-rain-icon.png"),
        27: Image.open("core/images/oblachno.png"),
        28: Image.open("core/images/oblachno.png"),
        30: Image.open("core/images/peremennaya_oblachnost.png"),
        32: Image.open('core/images/solnechno-sunny-icon .png'),
        34: Image.open('core/images/solnechno-sunny-icon .png'),
        47: Image.open("core/images/groza-thunder-lightning-storm-icon.png")
    }

    image1 = images[codeWaether[0]]
    image2 = images[codeWaether[1]]
    image3 = images[codeWaether[2]]
    image4 = images[codeWaether[3]]
    image5 = images[codeWaether[4]]
    image6 = images[codeWaether[5]]

    #resizing
    #overlay
    background.paste(image1, (250, 100), image1)
    background.paste(image2, (0, 500), image2)
    background.paste(image3, (128, 500), image3)
    background.paste(image4, (250, 500), image4)
    background.paste(image5, (370, 500), image5)
    background.paste(image6, (500, 500), image6)

    #save
    #display


    background.save("test.png")



    im = Image.open('test.png')
    draw = ImageDraw.Draw(im)
    fontsize = 70
    sizi = 50
    sizis = 35
    font = ImageFont.truetype("core/images/17680.otf", fontsize)
    fonts = ImageFont.truetype("core/images/17680.otf", sizi)
    today = ImageFont.truetype("core/images/17680.otf", sizis)
    draw.text((273, 48), str(tempNow), fill=(255,255,255), font=font)
    draw.text((0, 20),   f"Today in {city}", fill=(255,255,255), font=today)
    draw.text((0, 60),   f"Wind {wind} m/s", fill=(255,255,255), font=today)
    draw.text((0, 100),   f"humidity {humidity} %", fill=(255,255,255), font=today)
    draw.text((145, 350), day[2], fill=(255,255,255), font=fonts)
    draw.text((145, 400), f"{tempMax[2]}C", fill=(255,255,255), font=fonts)
    draw.text((145, 450), f"{tempMin[2]}C", fill=(255,255,255), font=fonts)
    draw.text((260, 350), day[3], fill=(255,255,255), font=fonts)
    draw.text((260, 400), f"{tempMax[3]}C", fill=(255,255,255), font=fonts)
    draw.text((260, 450), f"{tempMin[3]}C", fill=(255,255,255), font=fonts)
    draw.text((388, 350), day[4], fill=(255,255,255), font=fonts)
    draw.text((388, 400), f"{tempMax[4]}C", fill=(255,255,255), font=fonts)
    draw.text((388, 450), f"{tempMin[4]}C", fill=(255,255,255), font=fonts)

    draw.text((525, 350), day[5], fill=(255,255,255), font=fonts)
    draw.text((525, 400), f"{tempMax[5]}C", fill=(255,255,255), font=fonts)
    draw.text((525, 450), f"{tempMin[5]}C", fill=(255,255,255), font=fonts)

    draw.text((20, 350),   day[1], fill=(255,255,255), font=fonts)
    draw.text((20, 400),  f"{tempMax[1]}C", fill=(255,255,255), font=fonts)
    draw.text((20, 450),  f"{tempMin[1]}C", fill=(255,255,255), font=fonts)
    im.save("test.png")
#    im.save("test.jpg", "JPEG")

#crateImage("25 C",[32, 5, 9, 10,34,11],"Mon")

