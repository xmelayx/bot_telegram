import urllib.request, json

def courses():

    response = urllib.request.urlopen('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5')

    data = json.loads(response.read())
    print(data)
    dataUsd = data[0]
    dataUsdBuy = dataUsd["buy"]
    dataUsdsale = dataUsd["sale"]
    dataEur = data[1]
    dataEurBuy = dataEur["buy"]
    dataEurSale = dataEur["sale"]

    messageCourses = "💸Currency    💴  Buy      💶   Sale\n" \
                     f"💸USD:          {dataUsdBuy}    {dataUsdsale}\n" \
                     f"💶EUR:          {dataEurBuy}    {dataEurSale}"

    return messageCourses
