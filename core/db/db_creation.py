import postgresql
import os

postgres = os.environ["DB_TELEGRAM"]
db = postgresql.open(postgres)

def dbCreationForTelegram():
    try:
        db.execute("CREATE TABLE tg_users (id SERIAL PRIMARY KEY, "
                   "chat_id INTEGER, name CHAR(64), username CHAR(64), admin_user CHAR(64))")
        print("Table tg_users created")
    except:
        print("Table tg_users all exist")
    try:
        db.execute("CREATE TABLE tg_city (id SERIAL PRIMARY KEY, "
                   "chat_id INTEGER, city_name CHAR(64))")
        print("Table tg_city created")
    except:
        print("Table tg_city all exist")

dbCreationForTelegram()