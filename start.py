import core.telegramBot
import core.db.db_creation

core.db.db_creation.dbCreationForTelegram()
core.telegramBot.startTelegramBot()
